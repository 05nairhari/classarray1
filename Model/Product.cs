﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassArray.Model
{
    internal class Product
    {
        #region Properties
        //        //int id;

        //        //Properties
        //        public int Id { get; set; }
        //        public int properties { get; set; }
        //       //prop and 2 tabs
        //    }
        //}
        #endregion

        public string Name { get; set; }
        public string Category { get; set; }
        public int Price { get; set; }
        public float Rating { get; set; }

        public Product(string name, string category, int price, float rating)
        {
            Name = name;
            Category = category;
            Price = price;
            Rating = rating;
        }
        public override string ToString()
        {
            return $"Name::{Name}\t Category::{Category}\t Rating ::{Rating}\t Price::{Price}";
        }
    }
}