﻿using ClassArray.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassArray.Repository
{
    internal class ProductRepository
    {
        Product[] products;

        //Decl ClassArray
        public ProductRepository()
        {
            products = new Product[]
            {
                new Product("Samsung","Mobile",16000,4.4f),
                new Product("Vivo","Mobile",20000,4.1f),
                new Product("Dell" ,"Laptop",10000,4.3f),
                new Product("Poco","Mobile",14000,4.4f)
            };

        }

        public Product[] GetAllProducts()
        {
            return products;
        }
        public Product[] filterProduct()
        {
            return Array.FindAll(products, item => item.Category == "Mobile");
        }
        public Product[] DeleteProduct()
        {
            return Array.FindAll(products, item => item.Category != "Laptop");
        }



        public Product[] UpdateProduct(string category)
        {
            for (int i = 0; i < products.Length; i++)
            {
                if (products[i].Category == category)
                {
                    products[i].Category = "Hello";
                }
            }
            return products;
        }
    }
}

    

